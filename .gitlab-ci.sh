#!/bin/bash

# Install dependencies only for Docker.
[[ ! -e /.dockerinit ]] && [[ ! -e  /.dockerenv ]] && exit 0

# Enable modules
docker-php-ext-enable zip
docker-php-ext-enable pdo_mysql
docker-php-ext-enable soap
docker-php-ext-enable gd

# Install project dependencies.
composer install --no-suggest --prefer-dist
