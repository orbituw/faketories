<?php
require 'vendor/autoload.php';

return Madewithlove\PhpCsFixer\Config::fromFolders(['src'])->mergeRules([
    'psr0' => false,
    'ternary_to_null_coalescing' => false,
]);