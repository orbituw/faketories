<?php
/**
 * Created by PhpStorm.
 * User: talv
 * Date: 15/07/17
 * Time: 16:58.
 */
use Faker\Generator;


$faketory->define( OrbitUW\Faketories\Test\Example\VehiclesGateway::class, function(Generator $faker){
    return [
        'reg_no' => $faker->unique()->name,
        'make' => $faker->name(),
        'model' => $faker->numberBetween(100,400),
        'colour' => $faker->colorName,
        'year' => $faker->year,
        'engine' => $faker->numberBetween(500, 4000),
    ];
});