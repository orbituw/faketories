<?php
/**
 * Created by PhpStorm.
 * User: talv
 * Date: 15/07/17
 * Time: 16:13.
 */

return [
    /**
     * The path for faktories to be loaded from
     */
    'path' => env('FAKETORIES_PATH', base_path().'/tests/faketories/'),
];