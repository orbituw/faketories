<?php

namespace OrbitUW\Faketories\Commands;

use Illuminate\Console\Command;

class InstallFaketories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'faketories:install';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a sample faketories file within your applications test folder';

    /**
     * Execute the command.
     */
    public function handle()
    {
        if (file_exists(base_path('tests').'/faketories/faketories.php')) {
            $this->comment('It looks like faketories has already been installed. Please check your projects "/tests/faketories" folder');

            return;
        }

        if (!is_dir(base_path('tests'))) {
            $this->line('Creating "tests" folder...');
            $result = \File::makeDirectory(base_path('tests'), 0775);
            if (!$result) {
                $this->line('<error>[✘]</error> Failed creating '.(base_path('tests')).' folder');
            }
            $this->comment('Folder "tests" created.');
        }

        if (!is_dir(base_path('tests').'/faketories')) {
            $this->line('Creating "tests/faketories" folder...');
            $result = \File::makeDirectory(base_path('tests').'/faketories', 0775);
            if (!$result) {
                $this->line('<error>[✘]</error> Failed creating '.(base_path('tests').'/faketories').' folder');
                $this->line('Fix this by running \'chmod -R 755 tests/\'.'.PHP_EOL);
            }
            $this->comment('Folder "tests/faketories" created.');
        }

        $this->line('Copying sample faketories file...');
        $result = \File::copy(__DIR__.'/../../Example/faketories.php', base_path('tests/faketories/faketories.php'));
        if (!$result) {
            $this->line('<error>[✘]</error> Failed copying sample config file');
            $this->line('Fix this by running \'chmod -R 755 tests/\'.'.PHP_EOL);
        }
        $this->comment('Sample config copied.');
        $this->info('The faketories trait is ready to use.');
    }
}
