<?php
/**
 * Created by PhpStorm.
 * User: talv
 * Date: 12/07/2017
 * Time: 10:48.
 */

namespace OrbitUW\Faketories;

use Illuminate\Support\ServiceProvider;
use OrbitUW\Faketories\Commands\InstallFaketories;

class FaketoriesServiceProvider extends ServiceProvider
{
    protected $commands = [
        InstallFaketories::class,
    ];

    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        $configPath = __DIR__.'/../config/faketories.php';

        // Merge config files...
        $this->mergeConfigFrom($configPath, 'faketories');

        if ($this->app->runningInConsole()) {
            // Register the commands...
            $this->commands($this->commands);

            // Allow publishing the config file, with tag: config
            $this->publishes([$configPath => config_path('faketories.php')], 'config');
        }
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        //$this->mergeConfigFrom(__DIR__.'/../config/config.php', 'skeleton');
    }
}
