<?php
/**
 * Created by PhpStorm.
 * User: talv
 * Date: 12/07/2017
 * Time: 12:30.
 */

namespace OrbitUW\Faketories\Testing;

use Faker\Generator;
use Illuminate\Support\Collection;
use Symfony\Component\Finder\Finder;

// todo...
// x make factories file path configurable but also have a default file path
// x load files from a path instead of a single file
// x store faketory definitions within the trait
// x make the way we define faketory entries look like model factories
// x extract to package
// - add unit tests
// - add readme with examples

trait Faketories
{
    /**
     * @var Collection
     */
    protected $data;

    /**
     * @var array
     */
    private $definitions = [];

    /**
     * @param array $data
     * @param int   $times
     *
     * @return Collection
     */
    public function generate(array $data = [], $times = 1)
    {
        $this->load(config('faketories.path'));

        if (is_null($this->data)) {
            $this->data = collect();
        }

        $this->generateData($data, $times);

        return $this->data;
    }

    /**
     * @return Collection
     */
    public function getFaketoriesDefinitions()
    {
        return collect($this->definitions);
    }

    /**
     * @return Collection
     */
    public function getFaketoriesData()
    {
        return collect($this->data);
    }

    /**
     * @param array $overrides
     * @param $times
     */
    private function generateData(array $overrides, $times)
    {
        $faker = app(Generator::class);

        for ($i = 0; $i < $times; ++$i) {
            $generatedData = $this->definitions[get_called_class()]($faker);
            $aggregatedData = array_merge($generatedData, $overrides);
            $this->data->push((object) $aggregatedData);
        }
    }

    /**
     * Load factories from path.
     *
     * @param string $path
     *
     * @return $this
     */
    public function load($path)
    {
        $faketory = $this;

        if (is_dir($path)) {
            foreach (Finder::create()->files()->in($path) as $file) {
                require $file->getRealPath();
            }
        }

        return $faketory;
    }

    /**
     * @param $class
     * @param callable $attributes
     */
    private function define($class, callable $attributes)
    {
        $this->definitions[$class] = $attributes;
    }
}
