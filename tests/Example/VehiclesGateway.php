<?php
/**
 * Created by PhpStorm.
 * User: teejay
 * Date: 18/07/17
 * Time: 10:45
 */

namespace OrbitUW\Faketories\Test\Example;


use OrbitUW\Faketories\Testing\Faketories;

class VehiclesGateway
{
    use Faketories;

    public function getVehicleByRegNo($regNo)
    {
        return $this->data->filter(function($vehicleGateway) use($regNo) {
            return $vehicleGateway->reg_no === $regNo;
        })->first();
    }

}