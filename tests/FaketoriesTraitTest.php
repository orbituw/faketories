<?php
/**
 * Created by PhpStorm.
 * User: talv
 * Date: 17/07/2017
 * Time: 12:50
 */

namespace OrbitUW\Faketories\Test;

use OrbitUW\Faketories\Test\Example\VehiclesGateway;

/**
 * Class FaketoriesTraitTest
 * @package OrbitUW\Faketories\Test
 * @coversDefaultClass OrbitUW\Faketories\Test\Example\VehiclesGateway
 */
class FaketoriesTraitTest extends TestCase
{

    /**
     * @covers ::getFaketoriesDefinitions
     * @covers ::getFaketoriesDefinitions
     */
    public function test_definitions_can_be_loaded()
    {
        $vehicle = new VehiclesGateway();
        $vehicle->generate([
            'reg_no' => 've59hgp'
        ]);
        $vehicle->generate([ ], 5);

        $this->assertTrue($vehicle->getFaketoriesDefinitions()->has(VehiclesGateway::class));
    }

    /**
     * @covers ::getVehicleByRegNo
     */
    public function test_vehicle_reg_can_be_searched()
    {
        $vehicle = new VehiclesGateway();
        $vehicle->generate([
            'reg_no' => 've59hgp'
        ]);
        $vehicle->generate([ ], 5);

        $response = $vehicle->getVehicleByRegNo('ve59hgp');

        $this->assertEquals('ve59hgp', $response->reg_no);

    }

    /**
     * @covers ::load
     */
    public function test_generates_amount_of_vehicles_as_expected()
    {
        //arrange
        $fakeVehiclesGateway = new VehiclesGateway();

        //act
        $response = $fakeVehiclesGateway->generate([], 4);

        //assert gateway has a collection of data containing 4 items
        $this->assertCount(4, $response );
    }

    /**
     * @covers ::generate
     * @covers ::generateData
     */
    public function test_that_overrides_work()
    {
        $fakeVehiclesGateway = new VehiclesGateway();
        $response = $fakeVehiclesGateway->generate(['colour' => 'red']);
        $this->assertTrue(
            $response->first()->colour === 'red',
            'The override for colour did not work'
        );

    }

    /**
     * @covers ::getFaketoriesData
     */
    public function test_you_can_get_faketories_data()
    {
        $vehicle = new VehiclesGateway();
        $vehicle->generate(['reg_no' => 'bn54als']);

        $response = $vehicle->getFaketoriesData();

        $this->assertTrue(
          $response->first()->reg_no === 'bn54als'
        );

    }
}