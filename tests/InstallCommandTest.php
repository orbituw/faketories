<?php
/**
 * Created by PhpStorm.
 * User: talv
 * Date: 17/07/2017
 * Time: 12:10
 */

namespace OrbitUW\Faketories\Test;


use Artisan;

class InstallCommandTest extends TestCase
{

    public function tearDown()
    {
        $faketoriesSampleFile = base_path('tests/faketories/faketories.php');
        if( file_exists($faketoriesSampleFile)){
            unlink($faketoriesSampleFile);
            rmdir(base_path('tests/faketories'));
            rmdir(base_path('tests'));
        }
    }

    public function test_artisan_installs_faketories()
    {
        $this->artisan('faketories:install');

        $this->assertTrue( file_exists(base_path('tests/faketories/faketories.php')));

    }

    public function test_sample_faketories_file_doesnt_exist()
    {
        $this->assertTrue( !file_exists(base_path('tests/faketories/faketories.php')));
    }

    public function test_if_already_installed_user_is_notified()
    {
        $this->artisan('faketories:install');
        $this->artisan('faketories:install');

        $this->assertEquals("It looks like faketories has already been installed. Please check your projects \"/tests/faketories\" folder", trim(Artisan::output()));
    }
    
}