<?php

namespace OrbitUW\Faketories\Test;

class TestCase extends \Orchestra\Testbench\TestCase{

    protected function getPackageProviders($app)
    {
        return [
          \OrbitUW\Faketories\FaketoriesServiceProvider::class,
        ];
    }


    protected function getEnvironmentSetUp($app)
    {

        $app['config']->set('FAKETORIES_PATH', __DIR__.'/../Example');

    }

}